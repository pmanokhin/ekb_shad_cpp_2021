#!/usr/bin/python3

import argparse
import json
import os
import ssl
import subprocess
import sys
import tarfile
import zipfile
from shutil import copyfile
from urllib.request import urlopen

def povide_dependency(name, url, target):
    if not os.path.exists(target):
        print(f'Dwonloading {name} ...')

        archive_name = target + '.tgz'

        with urlopen(url, timeout=120, context=ssl._create_unverified_context()) as f:
            data = f.read()

        with open(archive_name, 'wb') as fd:
            fd.write(data)

        with tarfile.open(archive_name) as tar:
            tar.extractall(target)

        os.remove(archive_name)

def try_unpack_sources(src_dir, dest_dir):
    dir_content = os.listdir(src_dir)
    if len(dir_content) == 1:
        archive_name = os.path.join(src_dir, dir_content[0])

        if not os.path.isdir(archive_name):
            if tarfile.is_tarfile(archive_name):
                with tarfile.open(archive_name) as tar:
                    tar.extractall(dest_dir)
                    return dest_dir

            elif zipfile.is_zipfile(archive_name):
                with zipfile.ZipFile(archive_name) as zip:
                    zip.extractall(dest_dir)
                    return dest_dir

    return src_dir

def find_entry_point(src_dir):
    def work(src_dir):
        for entry in os.scandir(src_dir):
            if entry.is_file() and entry.name == 'entry_point.h':
                return src_dir
            if entry.is_dir():
                result = find_entry_point(os.path.join(src_dir, entry.name))
                if result:
                    return result
        return None
    
    return work(src_dir) or src_dir

def get_platform():
    if sys.platform.startswith('darwin'):
        return 'macos'
    elif sys.platform.startswith('linux'):
        return 'linux'
    elif sys.platform.startswith('win32') or sys.platform.startswith('cygwin'):
        return 'windows'
    else:
        raise RuntimeError('Unknown platform: {}'.format(sys.platform))


class Report:
    def __init__(self, status, brief = '', detailed = ''):
        self.status = status
        self.brief = brief
        self.detailed = detailed

def multiline(*lines):
    return '\n'.join(lines)

def clang_url(platform):
    def get_folder(platform):
        if platform == 'windows': return 'Win'
        elif platform == 'linux': return 'Linux_x64'
        elif platform == 'macos': return 'Mac'
        else: return ''

    return 'http://commondatastorage.googleapis.com/chromium-browser-clang/' \
        f'{get_folder(platform)}/' \
        'clang-llvmorg-13-init-9881-g5dad3d1b-1.tgz'

def clang_dir(tools_dir):
    return os.path.join(tools_dir, 'clang')

def clang_bin_path(platform, tools_dir):
    if platform == 'windows': 
        return os.path.join(clang_dir(tools_dir), 'bin', 'clang.exe')
    elif platform == 'linux': 
        return os.path.join(clang_dir(tools_dir), 'bin', 'clang++')
    elif platform == 'macos': 
        return os.path.join(clang_dir(tools_dir), 'bin', 'clang')
    else: 
        return ''

def fix_clang_bin(platform, tools_dir):
    if platform == 'windows' and not os.path.exists(clang_bin_path(platform, tools_dir)):
        copyfile(
            os.path.join(clang_dir(tools_dir), 'bin', 'clang-cl.exe'),
            clang_bin_path(platform, tools_dir)
        )

def gtest_path(tools_dir, *components):
    return os.path.join(tools_dir, 'gtest', 'googletest-release-1.11.0', *components)

def target_path(platform, work_dir):
    if platform == 'windows': return os.path.join(work_dir, 'tests.exe')
    elif platform == 'linux': return os.path.join(work_dir, 'tests')
    elif platform == 'macos': return os.path.join(work_dir, 'tests')
    else: return ''

def tests_out_path(work_dir):
    return os.path.join(work_dir, 'result.json')

def obj_path(work_dir, src_path):
    target_file_name, _ = os.path.splitext(os.path.basename(src_path))
    return os.path.join(work_dir, target_file_name + '.o')

def get_files(dir, predicate = lambda x : True):
    return [entry.path for entry in os.scandir(dir) if predicate(entry.path)]

def is_src(path):
    _, ext = os.path.splitext(path)
    return ext == '.cpp'

def sanitizers():
    return ['address', 'undefined']

def compile(print_messages, platfrom, tools_dir, options, compile_list):
    common_args = [clang_bin_path(platfrom, tools_dir)] + options
    for src_path, target_path in compile_list:
        command_args = common_args + ['-c', src_path, '-o', target_path]
        file_name = os.path.basename(src_path)

        if print_messages:
            print(f'Compiling {file_name} ...')

        result = subprocess.run(command_args, text=True, capture_output=True)

        if result.returncode != 0:
            return Report(
                False,
                f'Compilation error in {file_name}',
                multiline(result.stdout, result.stderr)
            )

    return Report(True)

def link(print_messages, platform, tools_dir, options, obj_list, target_name):
    command_args = [clang_bin_path(platform, tools_dir)]
    command_args += options
    command_args += [obj_file for obj_file in obj_list]
    command_args += ['-fuse-ld=lld', '-o', target_name]

    if print_messages:
        print('Linking ...')

    result = subprocess.run(command_args, text=True, capture_output=True)

    if result.returncode != 0:
        return Report(
            False,
            'Link error',
            multiline(result.stdout, result.stderr)
        )

    return Report(True)

def build_project(print_messages, platform, sanitizer, tools_dir, work_dir, src_dir, test_dir):
    sources = []
    sources += get_files(src_dir, is_src)
    sources += get_files(test_dir, is_src)
    sources += [
        gtest_path(tools_dir, 'googletest', 'src', 'gtest-all.cc'),
        gtest_path(tools_dir, 'googletest', 'src', 'gtest_main.cc'),
        gtest_path(tools_dir, 'googlemock', 'src', 'gmock-all.cc'),
    ]

    includes = [
        gtest_path(tools_dir, 'googletest'),
        gtest_path(tools_dir, 'googletest', 'include'),
        gtest_path(tools_dir, 'googlemock'),
        gtest_path(tools_dir, 'googlemock', 'include'),

        src_dir
    ]

    compiler_options = [
        f'-fsanitize={sanitizer}',
        '--std=c++17',
        '-Wall',
        '-Werror',
    ]
    compiler_options += [f'-I{path}' for path in includes]

    targets = [obj_path(work_dir, src_path) for src_path in sources]

    compile_result = compile(
        print_messages, platform, tools_dir, compiler_options, zip(sources, targets)
    )
    if not compile_result.status:
        return compile_result

    linker_options = [
        f'-fsanitize={sanitizer}'
    ]
    link_result = link(
        print_messages, platform, tools_dir, linker_options, targets, target_path(platform, work_dir)
    )
    if not link_result.status:
        return link_result

    return Report(True)

def clean_project(work_dir):
    if os.path.exists(work_dir):
        for file in get_files(work_dir):
            os.remove(file)
    else:
        os.mkdir(work_dir)

def run_project(print_messages, platform, work_dir):
    command_args = [
        target_path(platform, work_dir),
        f'--gtest_output=json:{tests_out_path(work_dir)}'
    ]
    
    if print_messages:
        print('running tests ...')

    result = subprocess.run(command_args, text=True, capture_output=True)

    if not os.path.exists(tests_out_path(work_dir)):
        return Report(
            False,
            'Possible crash while running tests',
            multiline(result.stdout, result.stderr)
        )

    with open(tests_out_path(work_dir), 'rt') as file:
        tests_data = json.load(file)

    def get_status(suite):
        return suite['failures'] + suite['errors'] == 0

    def status_string(suite):
        return f'{suite["name"]}: {"OK" if get_status(suite) else "FAIL"}'

    def failures(suites):
        for suite in suites:
            for test in suite['testsuite']:
                for failure in test.get('failures', []):
                    yield multiline(
                        f'{test["classname"]}, {test["name"]}',
                        failure['failure']
                    )

    return Report(
        all(get_status(suite) for suite in tests_data['testsuites']),
        ', '.join(status_string(suite) for suite in tests_data['testsuites']),
        '\n\n'.join(failures(tests_data['testsuites']))
    )

def do_project(print_messages, platform, tools_dir, work_dir, src_dir, test_dir):
    def run_with_sanitizer(sanitizer):
        clean_project(work_dir)

        build_result = build_project(
            print_messages, platform, sanitizer, tools_dir, work_dir, src_dir, test_dir
        )
        if not build_result.status:
            return build_result

        return run_project(print_messages, platform, work_dir)

    def brief(sanitizer, result):
        return f'run with {sanitizer} sanitizer: {result.brief}'

    def detailed(sanitizer, result):
        if result.detailed:
            return f'\n-----\ndetails of run with {sanitizer} sanitizer:\n {result.detailed}'
        return ''

    results = [(sanitizer, run_with_sanitizer(sanitizer)) for sanitizer in sanitizers()]

    return multiline(
        '\n'.join(brief(sanitizer, result) for sanitizer, result in results),
        ''.join(detailed(sanitizer, result) for sanitizer, result in results)
    )

def provide_tools(platform, tools_dir):
    if not os.path.exists(tools_dir):
        os.mkdir(tools_dir)

    povide_dependency(
        'clang',
        clang_url(platform),
        clang_dir(tools_dir)
    )
    fix_clang_bin(platform, tools_dir)

    povide_dependency(
        'gtest',
        'https://github.com/google/googletest/archive/refs/tags/release-1.11.0.tar.gz',
        os.path.join(tools_dir, 'gtest')
    )

def local_run(args):
    platform = get_platform()

    work_dir = os.path.abspath(args.work_dir)
    tools_dir = os.path.abspath(args.tools_dir)
    src_dir = os.path.abspath(args.sorces_dir)
    test_dir = os.path.abspath(args.tests_dir)

    provide_tools(platform, tools_dir)
    print(do_project(True, platform, tools_dir, work_dir, src_dir, test_dir))

def server_run(args):
    tasks_dir = os.path.join(os.path.dirname(os.path.abspath(__file__)), 'tasks')

    with open(os.path.join(tasks_dir, 'map.json'), 'rt') as f:
        task_tests = json.load(f)

    if args.task_name in task_tests:
        platform = get_platform()

        work_dir = '/work_dir'
        tools_dir = '/tools_dir'
        test_dir = os.path.join(tasks_dir, task_tests[args.task_name])
        src_dir = find_entry_point(try_unpack_sources(
            os.path.abspath(args.sorces_dir),
            '/src_dir'
        ))

        print(do_project(False, platform, tools_dir, work_dir, src_dir, test_dir))

    else:
        print('tests are not provided for this task')


def main():
    parser = argparse.ArgumentParser()
    subparsers = parser.add_subparsers(dest='command', required=True)

    local_parser = subparsers.add_parser('local')
    local_parser.add_argument('-t', '--tools-dir', default='tools_dir')
    local_parser.add_argument('-w', '--work-dir', default='work_dir')
    local_parser.add_argument('sorces_dir')
    local_parser.add_argument('tests_dir')

    server_parser = subparsers.add_parser('server')
    server_parser.add_argument('task_name')
    server_parser.add_argument('sorces_dir')

    args = parser.parse_args()
    if args.command == 'local': local_run(args)
    elif args.command == 'server': server_run(args)

if __name__ == '__main__':
    main()