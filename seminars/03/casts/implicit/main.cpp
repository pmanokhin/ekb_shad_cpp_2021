#include <iostream>

struct Foo {
    Foo(int _a) : a(_a) {}
    // explicit Foo(int _a) : a(_a) {}
    Foo(const Foo&) = default;

    int a = 0;
};

void func(const Foo& f) {
    std::cout << "func called, f.a: " << f.a << std::endl;
}

struct Parent {

};
// struct Child : private Parent {
struct Child : public Parent {

};

int main() {
    Foo f = Foo(1);
    func(2);
    // func(Foo(2));

    Child child;
    Parent& ref = child;

    return 0;
}