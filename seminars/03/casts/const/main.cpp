#include <iostream>

void changeStr(const char* s) {
    char* str = const_cast<char*>(s);
    str[0] = '2';
    str[1] = '1';
}

int main() {
    const char* literalPtr = "12";
    char arr[3] = "12";
    const char* arrPtr = arr;

    changeStr(literalPtr);
    std::cout << literalPtr << std::endl;

    changeStr(arrPtr);
    std::cout << arrPtr << std::endl;

    return 0;
}