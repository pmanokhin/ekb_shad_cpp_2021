#pragma once

#ifdef COMPILE_LIB
    #define PORT __declspec(dllexport)
#else
    #define PORT __declspec(dllimport)
#endif

extern "C" {
    PORT int func(int x);
    // PORT int func(int x, int y);
}
