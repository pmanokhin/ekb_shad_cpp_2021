#include <iostream>

int init() {
    std::cout << "before main" << std::endl;
    return 0;
}

int x = init();

int main() {
    std::cout << "main" << std::endl;
    return 0;
}