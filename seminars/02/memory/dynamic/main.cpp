#include <iostream>
#include <cstdlib>
#include <new.h>

void cstyle() {
    const size_t size = 10;
    int* ptr = static_cast<int*>(malloc(size * sizeof(int)));
    for (size_t i = 0; i < size; i++) {
        ptr[i] = i * i;
    }
    free(ptr);
    std::cout << *ptr << std::endl;
}

struct Foo {
    Foo() { std::cout << "create" <<std::endl; }
    ~Foo() { std::cout << "destroy" <<std::endl; }
};

void construct() {
    Foo* foo = static_cast<Foo*>(malloc(sizeof(Foo)));
    new (foo) Foo();
    foo->~Foo();
    free(foo);
}

void cppstyle() {
    Foo* foo = new Foo;
    delete foo;

    // Foo* foo = new Foo[3];
    // delete[] foo;

    // int* ptr = new int[3];
    // delete ptr;

    // Foo* foo = new Foo;
    // delete[] foo;
}

int main() {
    cstyle();
    std::cout << "happy end" << std::endl;
    return 0;
}