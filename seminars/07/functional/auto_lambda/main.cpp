#include <iostream>

int main() {
    int a = 2;
    auto l = [=] (auto x) { return a + x; };

    auto i = l(1);
    auto d = l(1.5);
    std::cout << "l(1): " << i << std::endl;
    std::cout << "l(1.5): " << d << std::endl;

    return 0;
}