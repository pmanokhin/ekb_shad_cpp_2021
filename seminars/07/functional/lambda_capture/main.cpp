#include <iostream>

int main() {
    int a = 2;
    using FuncPtr = void (*)();
    FuncPtr fptr = [] () { std::cout << "Hi!" << std::endl; };
    fptr();

    // fptr = [a] () { std::cout << "a : " << a << std::endl; } // error!
    auto simpleCapture = [=] () { std::cout << "a : " << a << std::endl; };
    simpleCapture();

    auto refCapture = [&a] () { a = 3; };
    refCapture();
    std::cout << "a after change from lambda : " << a << std::endl;

    auto changeCapture = [a] () mutable {
        a = 10;
        std::cout << "a from lambda : " << a << std::endl;
    };
    changeCapture();
    std::cout << "a after change lambda capture : " << a << std::endl;

    struct Foo {
        void func() {
            auto lambda = [this] () {
                field = 20;
            };
            lambda();
        }
        int field;
    };
    Foo foo{ 10 };
    foo.func();
    std::cout << "foo.feild after labda call " << foo.field << std::endl;

    auto initCapture = [x = a * a] () {
        std::cout << "x from lambda: " << x << std::endl;
    };
    initCapture();


    std::cout << "sizeof(fptr) : " << sizeof(fptr) << std::endl;
    std::cout << "sizeof(simpleCapture) : " << sizeof(simpleCapture) << std::endl;
    std::cout << "sizeof(refCapture) : " << sizeof(refCapture) << std::endl;
    std::cout << "sizeof(changeCapture) : " << sizeof(changeCapture) << std::endl;

    return 0;
}