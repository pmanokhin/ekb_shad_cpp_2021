#include <iostream>
#include <functional>

int main() {
    struct Add {
        int operator () (int b) const {
            return a + b;
        }
        int a;
    };
    struct Mul {
        int operator () (int b) const {
            return a * b;
        }
        int a;
    };

    Add add{ 2 };
    std::cout << "add(3): " << add(3) << std::endl;
    Mul mul{ 2 };
    std::cout << "mul(3) : " << mul(3) << std::endl;

    std::function<int(int)> func = Add{ 2 };
    std::cout << "func(3): " << func(3) << std::endl;
    func = Mul{ 2 };
    std::cout << "func(3) : " << func(3) << std::endl;

    return 0;
}