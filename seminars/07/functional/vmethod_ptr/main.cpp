#include <iostream>
#include <functional>

struct Base {
    virtual void action() {
        std::cout << "Base::action()" << std::endl;
    }
};
struct Child : public Base {
    void action() override {
        std::cout << "Child::action()" << std::endl;
    }
};

using VMethod = void (Base::*)();

int main() {
    VMethod method = &Base::action;

    Child child;
    Base& base = child;
    (base.*method)();

    std::function<void(Base&)> f = &Base::action;
    f(base);

    std::cout << "sizeof(VMethod): " << sizeof(VMethod) << std::endl;

    return 0;
}