#include <iostream>

int main() {
    auto a = 1;
    auto b = "";

    auto c = a;
    auto& d = a;
    const auto& e = a;
    auto* f = &a;
    auto&& g = 1;

    return 0;
}