#include <iostream>
#include <thread>
#include <mutex>
#include <chrono>

using namespace std::chrono_literals;

std::mutex m1;
std::mutex m2;

volatile int counter1 = 0;
volatile int counter2 = 0;

int main() {
    std::thread t1([] () {
        for (int i = 0; i < 10; i++) {
            std::lock_guard<std::mutex> lock1(m1);
            counter1++;
            std::this_thread::sleep_for(10ms);
            
            std::lock_guard<std::mutex> lock2(m2);

            counter2 += counter1;
        }
    });
    std::thread t2([] () {
        for (int i = 0; i < 10; i++) {
            std::lock_guard<std::mutex> lock2(m2);
            counter2++;
            std::this_thread::sleep_for(10ms);

            std::lock_guard<std::mutex> lock1(m1);

            counter1 += counter2;
        }
    });
    t1.join();
    t2.join();

    std::cout << counter1 << std::endl;
    std::cout << counter2 << std::endl;

    return 0;
}