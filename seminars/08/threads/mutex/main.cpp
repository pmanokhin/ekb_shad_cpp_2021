#include <iostream>
#include <thread>
#include <mutex>

constexpr size_t countersNum = 4;
volatile int counters[countersNum] = {};
std::mutex lockers[countersNum] = {};

int main() {
    auto func = [] () {
        for (size_t i = 0; i < 1000 * 1000; i++) {
            for (size_t j = 0; j < 4; j ++) {
                // std::lock_guard<std::mutex> lock(lockers[j]);
                lockers[j].lock();
                counters[j] = counters[j] + 1;
                lockers[j].unlock();
            }
        }
    };
    std::thread t1(func);
    std::thread t2(func);
    t1.join();
    t2.join();

    for (size_t j = 0; j < 4; j ++) {
        std::cout << counters[j] << std::endl;
    }

    return 0;
}