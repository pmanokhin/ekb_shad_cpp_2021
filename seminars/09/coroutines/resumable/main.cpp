#include <coroutine>
#include <iostream>

template <class Result> struct Generator;

template <class Result>
struct GeneratorPromise
{
    Generator<Result> get_return_object() {
        return Generator<Result>(std::coroutine_handle<GeneratorPromise>::from_promise(*this));
    }

    std::suspend_always initial_suspend() { return {}; }
    std::suspend_never final_suspend() noexcept { return {}; }
    void unhandled_exception() {}
    void return_void() {}

    std::suspend_always yield_value(Result newValue) {
        value = std::move(newValue);
        return {};
    }

    Result value;
};

template <class Result>
struct Generator {
    using promise_type = GeneratorPromise<Result>;
    using Handle = std::coroutine_handle<GeneratorPromise<Result>>;

    Generator(Handle _coroutine) : coroutine(_coroutine) {}
    Generator(Generator&& other) : coroutine(other.coroutine) {
        other.coroutine = {};
    }
    Generator(const Generator&) = delete;
    ~Generator() {
        if (coroutine) {
            coroutine.destroy();
        }
    }

    Result operator () () {
        coroutine.resume();
        return coroutine.promise().value;
    }

    Handle coroutine;
};

template <class F>
void run(size_t times, F&& f) {
    auto gen = f();
    for (size_t i = 0; i < times; i++) {
        std::cout << gen() << std::endl;
    }
}

void runSquares() {
    run(10, [] () -> Generator<int> {
        for (int i = 1; ; i++) {
            co_yield i * i;
        }
    });
}

void runFibonacci() {
    run(10, [] () -> Generator<int> {
        int prev = 0;
        int current = 1;
        while (true) {
            int newPrev = current;
            int newCurrent = current + prev;

            prev = newPrev;
            current = newCurrent;

            co_yield current;
        }
    });
}

int main() {
    runSquares();
    std::cout << "Happy end" << std::endl;
    return 0;
}