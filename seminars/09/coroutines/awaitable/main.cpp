#include <coroutine>
#include <iostream>
#include <functional>
#include <vector>

namespace exec {
    std::vector<std::function<void()>> funcs;

    void add(std::function<void()> func) {
        funcs.emplace_back(std::move(func));
    }
    void run() {
        std::vector<std::function<void()>> toExec = std::move(funcs);
        for (const auto& func : toExec) {
            func();
        }
    }
}

struct CountAwaiter {
    bool await_ready() noexcept { return false; }
    void await_suspend(std::coroutine_handle<> handle) noexcept {
        proceed(handle, counter);
    }
    void await_resume() noexcept {}

    static void proceed(std::coroutine_handle<> handle, size_t count) {
        if (count > 0) {
            exec::add([handle, count = count - 1] () {
                proceed(handle, count);
            });
        } else {
            handle();
        }
    }

    size_t counter = 0;
};

struct TaskPromise;

struct Task {
    using promise_type = TaskPromise;
};

struct TaskPromise
{
    Task get_return_object() {
        return {};
    }

    std::suspend_never initial_suspend() { return {}; }
    std::suspend_never final_suspend() noexcept { return {}; }
    void unhandled_exception() {}
    void return_void() {}
};

Task coro() {
    std::cout << "Coro start" << std::endl;
    co_await CountAwaiter{2};
    std::cout << "Coro body" << std::endl;
    co_await CountAwaiter{3};
    std::cout << "Coro end" << std::endl;
}

int main() {
    coro();
    for (size_t i = 0; i < 10; i++) {
        std::cout << "frame: " << i << std::endl;
        exec::run();
    }
    return 0;
}