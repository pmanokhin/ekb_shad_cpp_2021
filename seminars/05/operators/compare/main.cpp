#include <iostream>

struct Index {
    Index(int _primary, int _secondary) : primary(_primary), secondary(_secondary) {}
    Index(const Index& other) : primary(other.primary), secondary(other.secondary) {}

    Index& operator = (const Index& other) {
        primary = other.primary;
        secondary = other.secondary;
        return *this;
    } 

    bool operator == (const Index& other) const {
        return primary == other.primary && secondary == other.secondary;
    }
    bool operator != (const Index& other) const {
        return primary != other.primary || secondary != other.secondary;
    }
    bool operator < (const Index& other) const {
        return primary < other.primary || (primary == other.primary && secondary < other.secondary);
    }
    bool operator > (const Index& other) const {
        return primary > other.primary || (primary == other.primary && secondary > other.secondary);
    }

    int primary;
    int secondary;
};

int main() {
    std::cout << "(2, 5) == (2, 5): " << (Index(2, 5) == Index(2, 5)) << std::endl;
    std::cout << "(2, 5) != (5, 2): " << (Index(2, 5) != Index(5, 2)) << std::endl;
    std::cout << "(1, 1) < (1, 2): " << (Index(1, 1) < Index(1, 2)) << std::endl;
    std::cout << "(2, 0) > (1, 0): " << (Index(2, 0) > Index(1, 0)) << std::endl;
    return 0;
}