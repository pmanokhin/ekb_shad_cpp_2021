#include <iostream>

template <class T>
class DArray {
public:
    DArray(size_t _size) : 
        size(_size), data(new T[size]) 
    {}
    ~DArray() {
        delete[] data;
    }
    const T& get(size_t index) const {
        return data[index];
    }
    void set(size_t index, const T& value) {
        data[index] = value;
    }

    void printValues() {
        for (size_t i = 0; i < size; i++) {
            printElement(data[i]);
        }
    }
private:
    size_t size;
    T* data;
};

void printElement(int e) {
    std::cout << e << std::endl;
}
void printElement(const char* e) {
    std::cout << e << std::endl;
}

void test() {
    DArray<int> intArray(3);
    intArray.set(0, 1);
    intArray.set(1, 10.5f);
    intArray.set(2, 100);
    // intArray.set(0, "one");
    std::cout << "intArray:" << std::endl;
    intArray.printValues();

    DArray<const char*> strArray(3);
    strArray.set(0, "one");
    strArray.set(1, "two");
    strArray.set(2, "three");
    // strArray.set(0, 10);
    std::cout << "strArray:" << std::endl;
    strArray.printValues();
}

int main() {
    test();
    return 0;
}