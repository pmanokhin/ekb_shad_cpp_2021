#include <iostream>

template <class T, size_t size>
struct Array {
    T data[size];
};

int main() {
    Array<int, 2> ia1 = {1, 2};
    Array<int, 2> ia2 = ia1;
    Array<int, 3> ia3 = {1, 2, 3};
    // Array<int, 3> ia3 = ia1;

    Array<double, 2> da1 = {1.5, 2.5};

    std::cout << "sizeof(ia1.data): " << sizeof(ia1.data) << std::endl;
    std::cout << "sizeof(da1.data): " << sizeof(da1.data) << std::endl;

    return 0;
}