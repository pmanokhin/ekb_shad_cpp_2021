#include <iostream>

template <class T> size_t sizeOf();

template <>
size_t sizeOf<int>() {
    return 4;
}

template <>
size_t sizeOf<double>() {
    return 8;
}

int main() {
    std::cout << "sizeOf<int>() : " << sizeOf<int>() << std::endl;
    std::cout << "sizeOf<double>() : " << sizeOf<double>() << std::endl;

    return 0;
}