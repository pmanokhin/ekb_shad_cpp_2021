#include <iostream>

template <class T>
struct Bar {
    template <class U>
    static void action() {
        std::cout << "generic action of generic class" << std::endl;
    }
};

template <> template <class U>
void Bar<int>::action() {
    std::cout << "generic action of int class" << std::endl;
}

// template <class T> template <>
// void Bar<T>::action<int>() {
//     std::cout << "int action of generic class" << std::endl;
// }

template <> template <>
void Bar<int>::action<int>() {
    std::cout << "int action of int class" << std::endl;
}

int main() {
    Bar<float>::action<float>();
    Bar<float>::action<int>();
    Bar<int>::action<float>();
    Bar<int>::action<int>();

    return 0;
}