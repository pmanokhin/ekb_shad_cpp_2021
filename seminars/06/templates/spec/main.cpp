#include <iostream>

template <class T>
class DArray {
public:
    DArray(size_t _size) : 
        size(_size), data(new T[size]) 
    {}
    ~DArray() {
        delete[] data;
    }
    const T& get(size_t index) const {
        return data[index];
    }
    void set(size_t index, const T& value) {
        data[index] = value;
    }
private:
    size_t size;
    T* data;
};

template <>
class DArray<bool> {
public:
    static const size_t baseSize = 64;

    DArray(size_t _size) : 
        size(_size), 
        data(new uint64_t[size % baseSize == 0 ? size / baseSize : size / baseSize + 1]) 
    {}
    ~DArray() {
        delete[] data;
    }
    bool get(size_t index) const {
        return (data[index / baseSize] >> (index % baseSize)) & 0x01;
    }
    void set(size_t index, bool value) {
        if (value) {
            data[index / baseSize] |= (uint64_t(1) << (index % baseSize));
        } else {
            data[index / baseSize] &= ~(uint64_t(1) << (index % baseSize));
        }
    }
private:
    size_t size;
    uint64_t* data;
};

int main() {
    const size_t size = 140;
    DArray<int> intArray(size);
    for (size_t i = 0; i < size; i++) {
        intArray.set(i, 2 * i);
    }
    bool intTest = true;
    for (size_t i = 0; i < size; i++) {
        intTest = intTest && intArray.get(i) == 2 * i;
    }
    std::cout << "intTest : " << intTest << std::endl;

    DArray<bool> boolArray(size);
    for (size_t i = 0; i < size; i++) {
        boolArray.set(i, i % 3 == 0);
    }
    bool boolTest = true;
    for (size_t i = 0; i < size; i++) {
        boolTest = boolTest && boolArray.get(i) == (i % 3 == 0);
    }
    std::cout << "boolTest : " << boolTest << std::endl;

    return 0;
}