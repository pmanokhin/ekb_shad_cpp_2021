#include <iostream>

struct Foo {
    Foo() {
        std::cout << "Foo::Foo()" << std::endl;
    }
    Foo(const Foo&) {
        std::cout << "Foo::Foo(const Foo&)" << std::endl;
    }
    Foo(Foo&&) {
        std::cout << "Foo::Foo(Foo&&)" << std::endl;
    }
};

void func(Foo&&) {
    std::cout << "func(Foo&&)" << std::endl;
}
void func(const Foo&) {
    std::cout << "func(const Foo&)" << std::endl;
}
void func(Foo&) {
    std::cout << "func(Foo&)" << std::endl;
}

template <class T>
void pass(T&& t) {
    func(t);
}

int main() {
    const Foo cf;
    pass(cf);
    Foo f;
    pass(f);
    pass(Foo());
    return 0;
}