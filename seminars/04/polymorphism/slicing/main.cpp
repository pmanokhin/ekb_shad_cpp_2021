#include <iostream>


struct Parent {
    virtual void method() const {
        std::cout << "Parent method was called" << std::endl;
    }
};
struct Child : public Parent {
    void method() const {
        std::cout << "Child method was called" << std::endl;
    }
};

void call(Parent p) {
    p.method();
}

int main() {
    call(Child());
    return 0;
}