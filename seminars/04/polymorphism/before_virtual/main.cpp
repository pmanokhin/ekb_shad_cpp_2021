#include <iostream>

struct Parent {
    void method() {
        std::cout << "Parent method was called" << std::endl;
    }
};
struct Child : public Parent {
    void method() {
        std::cout << "Child method was called" << std::endl;
    }
};

int main() {
    Child child;
    Parent& ref = child;
    ref.method();
    std::cout << "sizeof(Parent): " << sizeof(Parent) << std::endl;
    return 0;
}